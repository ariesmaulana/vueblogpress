module.exports = {
  title: "ARSMP",
  description: "Mencoba Vuepress",
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "Post", link: "/posts/" },
      { text: "About", link: "/about/" }
    ],
    sidebar: {
      "/posts/": sidebarConfig("Posts", ["", "new-post"]),
      "/about/": sidebarConfig("About", ["", "tentang-web"])
    }
  }
};

function sidebarConfig(title, child) {
  return [
    {
      title,
      collapsable: true,
      children: child
    }
  ]
}
